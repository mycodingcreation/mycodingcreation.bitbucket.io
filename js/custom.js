var myOptions = {
    autoplay: true,
    autoplaySpeed: 5000,
    dots: true,
    prevArrow:'<i class="fa fa-chevron-left slick-prev" aria-hidden="true"></i>',
    nextArrow:'<i class="fa fa-chevron-right slick-next" aria-hidden="true"></i>',

}

$('.mySlider').slick(myOptions);


